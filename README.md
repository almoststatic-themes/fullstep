# Fullstep

---
**This is a discontinued project. Try [flatstep](https://gitlab.com/almoststatic-themes/flatstep/) instead.**

But if you want, it is a working project that you can examine.

---

**Fullstep** is a theme for [Almoststatic](https://pypi.org/project/almoststatic/), see the [documentation](https://almoststatic.readthedocs.io)

Fullstep uses [Bootstrap](https://getbootstrap.com/) as css engine, and let you to compile your own customized version with sass.

Fullstep uses [Bootswatch](https://bootswatch.com/) as base css configuration and writes css theme starting from it.

## Quick start

This tutorial is tested on Linux, adjust it if you are on Windows or Mac.

Before you start, you need some programming tools on your pc:

- [Python](https://www.python.org/)
- [sass](https://sass-lang.com/)

You don't need to compile bootstrap, but if you like to do so, you need:

- [nodejs](https://nodejs.org)
- [npm](https://www.npmjs.com/)

A good programmer editor or ide is not required but advised, there are many on web, my choose is [Visual studio code](https://code.visualstudio.com/)

Install everything and go on.

I advise to make a folder for all your Almoststatic themes and within it make an Python environment similar to one described [here](https://almoststatic.readthedocs.io/en/latest/quickstart.html).

Copy into it the `fullstep` folder.

Then download [Bootstrap source files](https://getbootstrap.com/docs/5.0/getting-started/download/) and extract the files on folder `bootstrap`

Now your folder should look like this

```
   ├── bootstrap
   │   └── <...>
   ├── fullstep
   │   ├── content
   │   │   ├── media
   │   │   ├── pages
   │   │   ├── scss
   │   │   ├── templates
   │   │   └── config.yaml
   │   ├── custom
   │   │   └── <...>
   │   ├── flaskapp.py
   │   └── write_static.py
   └── myvenv
       └── <...>
```

And you are ready to start. Before all, goto on your main folder and run:

```
$ python3 -m venv myvenv
$ source myvenv/bin/activate
$ pip install -U pip
$ pip install almoststatic
$ pip install pyftpsync
```

Now you need to compile the css with sass, from your main folder:

```
$ cd fullstep/content/scss
$ ./compile_bootstrap.sh &
$ ./compile_main.sh &
```
If everything is ok, now you will see some `*.css` files on your `content/media/css` folder, else you have to fix your environment. And each time you change something in you `scss` files they will be recompiled

Now you are ready to develop your site. From main folder:

```
$ cd fullstep
$ python flaskapp.py
```
And navigate to `http://127.0.0.1:5000/` you should see the site and you can customize it with your contents.

When you are done, you can customize the `write_static.py` to write your static site.
## Develop derived sites

Fullstep comes with it's own theme. When you develop your site you need to write your contents and add your madia static files.

But often you need also to change some css from standard and to write some templates to adjust headers, footers and so on.

This is easy with Fullstep, the standard distribution contain an example. Simply go to custom folder, recompile css and run flaskapp. From main folder:

```
$ source myvenv/bin/activate
$ cd fullstep/custom/content/scss/
$ ./compile_bootstrap.sh &
$ ./compile_main.sh &
$ cd ../../
$ python flaskapp.py
```
And navigating to `http://127.0.0.1:5000/` you should see a derived site.

But to keep things ordered, could be a good idea to put sites outside Fullstep, for example:

```
   ├── bootstrap
   │   └── <...>
   ├── fullstep
   │   └── <...>
   ├── mysites
   │   ├── site1
   │   │   └── <...>
   │   └── site2
   │       └── <...>
   └── myvenv
       └── <...>
```

To start a new site copy `custom` folder from `fullstep` to a location of your choice, i.e. `mysites/site1`.

Now you have to adjust paths into your `content/config.yaml` file, write:

```
config:
  templates: ["custom_templates", "../../fullstep/templates"]
  static_url: "/"
  cache: False
```
This teach to Flask and Jinja2 to search templates first in `custom_templates`
and if the files is not found in '../../fullstep/templates'

Then you have to adjust paths into scss source files. In `main.scss` change the row:
```
@import "../../../content/scss/theme/widgets";
```
to
```
@import "../../../../fullstep/content/scss/theme/widgets";
```

In `custom_bootstrap.scss` the row:
```
@import "../../../../bootstrap/scss/bootstrap";
```
should be ok, but if needed change it to the one which is ok.

Off course, you have to tune relative paths until they are all correct.

When all is ok, you can recompile css and launch `flaskapp.py` as usual and start to add your contents and your style.

Enjoy!.
