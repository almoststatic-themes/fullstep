<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require "PHPMailer/PHPMailer.php";
require "PHPMailer/Exception.php";
require "PHPMailer/SMTP.php";

// configure with your data
$fromEmail = 'info@yourdomain.com';
$fromName = 'YourName';
$sendToEmail = 'info@yourdomain.com';
$sendToName = 'YourName contact form';
$subject = 'Message from yourdomain.com';
$okMessage = 'Thanks, we will reply asap.';
$noCaptcha = 'Wrong captcha id!';
$errorMessage = "Oops we got an error, try later.";
$emailTextHtml = "";

// configure with your data
$mail = new PHPMailer(true);
$mail->isSMTP();
$mail->Host       = '';
$mail->SMTPAuth   = true;
$mail->Username   = '';
$mail->Password   = '';
$mail->SMTPSecure = 'ssl';
$mail->Port       = 465;

// if you are not debugging and don't need error reporting, turn this off by error_reporting(0);
error_reporting(E_ALL & ~E_NOTICE);

// read captcha index
$file = '../captcha/index.json';
$data = json_decode(file_get_contents($file), TRUE);

function postval($searc) {
    foreach ($_POST as $key => $value) {
        if (endsWith($key, $searc)) {
            return $value;
        }
    }
    return "__not_found__";
}
function endsWith($key, $searc) {
    return substr_compare($key, $searc, -strlen($searc)) === 0;
}


try
{
    if(count($_POST) == 0) throw new \Exception('Form is empty');

    $id = postval('captcha-id');
    $captcha = $data[$id];
    $check = strtoupper(postval('captcha-check'));
    if($captcha !== $check) throw new \Exception($noCaptcha);

    $emailTextHtml .= "<table>\n";

    foreach ($_POST as $key => $value) {
        $emailTextHtml .= "<tr><th>$key</th><td>$value</td></tr>\n";
    }
    $emailTextHtml .= "</table>\n";

    $mail->setFrom($fromEmail, $fromName);
    $mail->addAddress($sendToEmail, $sendToName);

    $mail->isHTML(true);

    $mail->Subject = $subject;
    $mail->msgHTML($emailTextHtml);

    if(!$mail->send()) {
        throw new \Exception('I could not send the email.' . $mail->ErrorInfo);
    }

    $responseArray = array('type' => 'success', 'message' => $okMessage);

    $file = 'contact.log';
    $value  = "==============================================\n";
    $value .= date("Y-m-d G:i") . "\n";
    $value .= $responseArray['message'] . "\n";
    $value .= "==============================================\n";
    $value .= $emailTextHtml . "\n\n";

    file_put_contents($file, $value, FILE_APPEND | LOCK_EX);

}
catch (\Exception $e)
{
    $responseArray = array('type' => 'danger', 'message' => $e->getMessage());
}


// if requested by AJAX request return JSON response
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $encoded = json_encode($responseArray);

    header('Content-Type: application/json');

    echo $encoded;
}
// else just display the message
else {
    echo $responseArray['message'];
}
